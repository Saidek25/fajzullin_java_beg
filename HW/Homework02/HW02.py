print("Введите число от 0 до 100")
num = int(input())
if num < 0 or num > 100:
    print(num)
    print("Ошибка")
elif num >= 10 and num <= 20:
    if num % 2 == 0:
        num /= 2
        print(num)
    else:
        num *= 2
        print(num)
else:
    num *= 2
    print(num)
