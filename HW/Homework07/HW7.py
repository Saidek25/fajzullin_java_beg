def get_list(count: int):
    list = []
    for i in range(count):
        list.append(int(input("Введите элемент массива - ")))
    return list


def BubbleSorting(lst: list):
    size = len(lst)
    list = get_list
    for elem in range(size-1):
        for j in range(size - elem - 1):
            if lst[j] > lst[j + 1]:
                lst[j], lst[j + 1] = lst[j + 1], lst[j]
    return lst

if __name__ == '__main__':

    list = get_list(int(input("Введите кол-во элементов - ")))
    list = BubbleSorting(list)
    print(list)

