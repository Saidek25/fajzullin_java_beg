def get_array(count: int):
    list = []
    for i in range(count):
        list.append(int(input("Введите элемент - ")))
    return list


def get_max(array: list):
    list = get_array
    max2 = max(array)
    return max2


def get_min(array: list):
    list = get_array
    min2 = min(array)
    return min2


def get_sum(array: list):
    list = get_array
    sum = 0
    for i in array:
        sum += i
    return sum


def factorial(elem):
    fact = 1
    for elem in range(2, elem + 1):
        fact *= elem
    return fact


def factorials_to_set(lst: list) -> set:
    facts = set()
    for elem in lst:
        facts.add(factorial(elem))
    return facts


if __name__ == '__main__':
    list = get_array(int(input("Введите кол-во элементов - ")))
    print(list)
    summ = get_sum(list)
    print("Сумма - ", summ)
    minn = get_min(list)
    print("Минимальный - ", minn)
    maxx = get_max(list)
    print("Максимальный - ", maxx)
    facts = factorials_to_set(list)
    print("Факториалы - ", facts)
