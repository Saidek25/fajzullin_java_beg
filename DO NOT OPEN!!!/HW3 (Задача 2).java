package HomeWorks;

import java.util.Scanner;

public class HW03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите целое число от 0 до 20");
        int num = sc.nextInt();

        if (num < 0 | num > 20) {
            System.out.println("Ошибка! Попробуйте еще раз и введите число от 0 до 20)");
        } else {
            switch (num) {
                case (0):
                    System.out.println("Ноль");
                    break;
                case (1):
                    System.out.println("Один");
                    break;
                case (2):
                    System.out.println("Два");
                    break;
                case (3):
                    System.out.println("Три");
                    break;
                case (4):
                    System.out.println("Четыре");
                    break;
                case (5):
                    System.out.println("Пять");
                    break;
                case (6):
                    System.out.println("Шесть");
                    break;
                case (7):
                    System.out.println("Семь");
                    break;
                case (8):
                    System.out.println("Восемь");
                    break;
                case (9):
                    System.out.println("Девять");
                    break;
                case (10):
                    System.out.println("Десять");
                    break;
                default:
                    System.out.println("Вторая десятка");
            }

        }


    }
}