public class RationalFraction {

    private int numerator;
    private int denominator;

    public RationalFraction() {
    }

    public RationalFraction(int numerator, int denominator) {
        this.numerator = numerator;
        int number = numerator;
        this.denominator = denominator;
    }

    public String toString() {
        return numerator + "/" + denominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void reduce() {
        int limit = Math.min(getDenominator(), getNumerator());
        for (int i = limit; i > 0; i++) {
            if (getNumerator() % i == 0 && getDenominator() % i == 0) {
                this.numerator = this.numerator / i;
                this.denominator = this.denominator / i;
            }
        }
    }

    public RationalFraction mult(RationalFraction rationFractionOne) {
        RationalFraction rationFractionTwo = new RationalFraction(this.numerator * rationFractionOne.getNumerator(),
                this.denominator * rationFractionOne.getDenominator());
        rationFractionTwo.reduce();
        return rationFractionTwo;
    }

    public void mult2(RationalFraction rationFractionOne) {
        this.numerator *= rationFractionOne.numerator;
        this.denominator *= rationFractionOne.denominator;
    }

    public RationalFraction div(RationalFraction rationFractionOne) {
        RationalFraction rationFractionTwo = new RationalFraction(this.numerator * rationFractionOne.getDenominator(),
                this.denominator * rationFractionOne.getNumerator());
        rationFractionTwo.reduce();
        return rationFractionTwo;
    }

    public void div2(RationalFraction rationFractionOne) {
        this.numerator *= rationFractionOne.getDenominator();
        this.denominator *= rationFractionOne.getNumerator();
    }

    public double value() {
        return this.numerator / this.denominator;
    }
}