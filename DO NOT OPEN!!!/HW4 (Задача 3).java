import java.util.Scanner;

public class HW004 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите целое положительное число");
        Scanner s = new Scanner(System.in);
        int numOfSide = s.nextInt();
        int space = numOfSide - 1;
        for (int i = 0; i < numOfSide; i++) {

            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            space--;

            for (int j = 0; j < numOfSide; j++) {
                System.out.print("☺ ");
            }
            System.out.println();
        }

    }
}