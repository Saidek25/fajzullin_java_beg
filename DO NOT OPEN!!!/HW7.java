public class HW7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Добро пожаловать!");
        System.out.println("Введите размер массива: ");
       
        int arrSize = sc.nextInt();
        int[] array = getArr(arrSize, sc);
        
        System.out.println("Ваш массив: " + toString(array));
        System.out.println("Размер массива: " + arrSize);
        System.out.println("Сумма элеметнов массива: " + arrSum(array));
        bubbleSortedArr(array);
        maxElement(array);
        minElement(array);

    }

    public static int[] getArr(int arrSize, Scanner sc) {
        int[] array = new int[arrSize];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите элемент массива: ");
            array[i] = sc.nextInt();
        }
        return array;
    }

    public static String toString(int[] array) {
        String total = "(";
        for (int i = 0; i < array.length; i++) {
            total += array[i] + ", ";
        }
        total += "\b\b)";
        return total;
    }

    public static int arrSum(int[] array) {
        int sum = array[0];
        for (int i = 1; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    public static int[] bubbleSortedArr(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int x = array[j];
                    array[j] = array[i];
                    array[i] = x;
                }
            }
        }

        String sorted = "Отсортированный массив: ( ";
        for (int x : array) {
            sorted += x + ", ";
        }
        sorted += "\b\b )";
        System.out.println(sorted);
        return array;
    }

    public static void maxElement(int[] array) {
        System.out.println("Наибольший элемент массива: " + array[array.length - 1]);
    }

    public static void minElement(int[] array) {
        System.out.println("Наименьший элемент массива: " + array[0]);
    }
}

