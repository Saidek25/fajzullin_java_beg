package HomeWorks;

import java.util.Scanner;

public class HW9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Добро пожаловать!");
        System.out.println("Введите размер массива: ");
        int arrSize = sc.nextInt();
        int[] array = getArr(arrSize, sc);
        System.out.println("Ваш массив: " + toString(array));
        double[] doubleArr = getDoubleArr(arrSize, sc);
        System.out.println("Ваш дробный массив: " + toString(doubleArr));

        arrInPow(array);
        System.out.println("Ваш массив возведённый в степень: " + toString(array));

        arrInPow(doubleArr);
        System.out.println("Ваш дробный массив возведённый в степень: " + toString(doubleArr));

        System.out.println(getInt(sc));
        System.out.println(getDouble(sc));

    }

    public static int[] arrInPow(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] *= array[i];
        }
        return array;
    }

    public static double[] arrInPow(double[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] *= array[i];
        }
        return array;
    }

    public static int getInt(Scanner sc) {
        System.out.println("Введите целое число: ");
        int num = sc.nextInt();
        return num * num;
    }

    public static double getDouble(Scanner sc) {
        System.out.println("Введите дробное число: ");
        double drob = sc.nextDouble();
        return drob * drob;
    }

    public static String toString(int[] array) {
        String total = "(";
        for (int i = 0; i < array.length; i++) {
            total += array[i] + ", ";
        }
        total += "\b\b)";
        return total;
    }

    public static String toString(double[] array) {
        String total = "(";
        for (int i = 0; i < array.length; i++) {
            total += array[i] + ", ";
        }
        total += "\b\b)";
        return total;
    }

    public static int[] getArr(int arrSize, Scanner sc) {
        int[] array = new int[arrSize];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите элемент массива: ");
            array[i] = sc.nextInt();
        }
        return array;
    }

    public static double[] getDoubleArr(int arrSize, Scanner sc) {
        double[] array = new double[arrSize];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите дробный элемент массива: ");
            array[i] = sc.nextDouble();
        }
        return array;
    }

}