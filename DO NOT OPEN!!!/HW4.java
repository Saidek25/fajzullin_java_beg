package HomeWorks;

import java.util.Scanner;

public class HW4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите любое трёхзначное число ");
        int i = scanner.nextInt();

        if ((i - 100) > - 1 & (i - 100) < 900) {
            int a = i % 10;
            int b = ((i - a) / 10) % 10;
            int c = (((i - a) / 10) - b) / 10;
            int sum = a + b + c;
            System.out.println(sum);
        } else {
            System.out.println("Ошибка! Повторите попытку и введите ТРЁХЗНАЧНОЕ число!!!");
        }
    }
}