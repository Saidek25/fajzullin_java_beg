package HomeWorks;

import java.util.Scanner;

public class HW6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Доброго времени суток!");
        System.out.println("Введите количество элементов");
        int size = scanner.nextInt();
        int[] array = new int[size];    //TODO: выдаёт ошибку при указывании типа double
        System.out.println("Отлично, а теперь введите элементы массива:");

        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите элемент: ");
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {

                if (array[j] < array[i]) {
                    int q = array[i];
                    array[i] = array[j];
                    array[j] = q;
                }
            }
        }

        int max = array[0];
        for (int i = 0; i < array.length; i++) {

            if (array[i] > max) {
                max = array[i];
            }
        }
        System.out.println("Наибольший элемент: " + max);

        System.out.println("Отсортированный массив: ");
        for (int x : array) {
            System.out.println(x);
        }
    }
}