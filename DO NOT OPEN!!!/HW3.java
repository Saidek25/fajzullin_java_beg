package HomeWorks;

import java.util.Scanner;

public class HW3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число от 0 до 100");
        double number = sc.nextDouble();

        if (number < 0 || number > 100) {
            System.out.println("Ошибка! Запустите программу повторно. Подсказка: Вводимое число должно быть в диапазоне от 0 до 100!!!");
        } else {

            number = number < 10 & number > 20 ? (number *= 2) : (number % 2) == 0 ? number /= 2 : (number *=2);
            System.out.println(number);
        }

    }
}