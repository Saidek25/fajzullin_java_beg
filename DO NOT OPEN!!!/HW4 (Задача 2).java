package HomeWorks;

import java.util.Scanner;

public class HW04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите целое положительное число");
        int numOfSide = sc.nextInt();

        for (int i = 0; i < numOfSide; i++) {
            for (int j = 0; j < numOfSide; j++) {
                System.out.print("♫ ");
            }
            System.out.println();
        }
    }
}